
DROP TABLE IF EXISTS `product_categories`;

CREATE TABLE `product_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;



insert  into `product_categories`(`id`,`name`,`created_date`) values 
(1,'peralatan Mandi','2018-11-10 11:52:56'),
(2,'Minuman Kemasan','2018-11-10 11:52:59'),
(3,'Produk Susu','2018-11-10 11:53:01');



DROP TABLE IF EXISTS `products`;

CREATE TABLE `products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;



insert  into `products`(`id`,`name`,`category_id`,`created_date`) values 
(1,'sabun wangi',1,'2018-11-10 11:53:48'),
(2,'Minuman Cola',2,'2018-11-10 11:53:51'),
(3,'Prenagon Gold',3,'2018-11-10 11:53:53'),
(4,'Aquaa',2,'2018-11-10 11:53:56'),
(5,'The Botol ',2,'2018-11-10 11:53:58'),
(6,'Sampo',1,'2018-11-10 11:53:59');


