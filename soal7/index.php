<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

<?php

$servername = "localhost";
$username = "root";
$password = "";
$dbname = "test";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

?>
<div class="container">
  <h2>Contoh Tampilan</h2>
  <p>Ini adalah Tampilan Test</p>
  <table class="table">
    <thead>
      <tr >
        <th>id</th>
        <th>Name</th>
        <th>Jumlah</th>
      </tr>
    </thead>
    <tbody>
<?php
$sql = "SELECT a.id, a.name, COUNT(b.category_id) as jumlah
        FROM product_categories a
        JOIN products b ON a.id = b.category_id
        GROUP BY a.id";
$result = $conn->query($sql);

if ($result->num_rows > 0) {
    // output data of each row
    while($row = $result->fetch_assoc()) { ?>
        <tr class="info">
          <td><?php echo $row["id"]; ?></td>
          <td><?php echo $row["name"]; ?></td>
          <td><?php echo $row["jumlah"]; ?></td>
        </tr>
<?php
    }
} else {
    echo "0 results";
}
$conn->close();

 ?>

    </tbody>
  </table>
</div>

</body>
</html>
